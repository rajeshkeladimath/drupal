files_dir = "#{release_path}/docroot/sites/default/files"
shared_dir = "/mnt/shared/#{new_resource.name}/files"

directory "/mnt/shared/#{new_resource.name}"

bash "move files directory" do
    code <<-EOH
    mv #{files_dir} #{shared_dir}
EOH
    only_if { ::File.directory?(files_dir) }
    not_if { ::File.directory?(shared_dir) }
end

bash "rm sites directory" do
    code <<-EOH
    rm -rf #{files_dir}
EOH
    not_if { ::File.symlink?(files_dir) }
end

directory shared_dir

link files_dir do
    to shared_dir
end
